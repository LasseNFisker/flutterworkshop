import 'package:flutter/material.dart';
import 'package:workshop/pokemon.model.dart';

class PokemonDetailsPage extends StatelessWidget {
  final Pokemon pokemon;

  const PokemonDetailsPage({Key key, @required this.pokemon}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return new Scaffold(
      appBar: AppBar(),
      body: Container(
        padding: EdgeInsets.symmetric(horizontal: 25, vertical: 20),
        color: Colors.blueGrey.shade300,
        child: SizedBox.expand(
            child: SafeArea(
          child: Column(
            children: <Widget>[
              Hero(
                tag: pokemon.id,
                child: Container(
                    margin: EdgeInsets.symmetric(vertical: 20),
                    height: 200,
                    width: 200,
                    decoration: BoxDecoration(
                      color: Colors.blueGrey.shade600,
                      shape: BoxShape.circle,
                      image: DecorationImage(
                          fit: BoxFit.fill, image: NetworkImage(pokemon.img)),
                    )),
              ),
              _Info(
                label: "Name",
                value: pokemon.name,
              ),
              _Info(
                label: "Number",
                value: pokemon.number,
              ),
            ],
          ),
        )),
      ),
    );
  }
}

class _Info extends StatelessWidget {
  final String label;
  final String value;

  const _Info({Key key, @required this.label, @required this.value})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Row(
      mainAxisSize: MainAxisSize.max,
      mainAxisAlignment: MainAxisAlignment.spaceBetween,
      children: <Widget>[Text(label), Text(value)],
    );
  }
}
