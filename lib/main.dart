import 'package:flutter/material.dart';
import 'package:workshop/pokemon.model.dart';
import 'package:workshop/pokemon.service.dart';
import 'package:workshop/pokemon.widget.dart';

void main() => runApp(MyApp());

class MyApp extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    return MyAppState();
  }
}

class MyAppState extends State<MyApp> {
  bool searching = false;

  Widget _searchField = TextField(
    onChanged: _onSearch,
  );

  static _onSearch(String term) {
    print(term);
    pokemonService.filterPokemons(term);
  }

  @override
  Widget build(BuildContext context) {
    return new MaterialApp(
      theme: ThemeData(primaryColor: Colors.blueGrey),
      home: Scaffold(
        appBar: AppBar(
          leading: IconButton(
            icon: Icon(Icons.search),
            onPressed: () => setState(() {
                  searching = !searching;
                }),
          ),
          title: searching ? _searchField : Text("PokeDex"),
        ),
        body: Container(
          color: Colors.blueGrey.shade700,
          child: SafeArea(
            child: StreamBuilder<List<Pokemon>>(
              stream: pokemonService.pokemons$,
              builder: (context, pokemonsSnapshot) => pokemonsSnapshot.hasData
                  ? GridView.builder(
                      gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
                          crossAxisCount: 2),
                      itemCount: pokemonsSnapshot.data.length,
                      itemBuilder: (context, i) =>
                          AppPokemon(pokemon: pokemonsSnapshot.data[i]))
                  : Center(child: CircularProgressIndicator()),
            ),
          ),
        ),
      ),
    );
  }
}
