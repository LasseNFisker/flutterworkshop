import 'package:flutter/material.dart';
import "package:json_annotation/json_annotation.dart";

part "pokemon.model.g.dart";

// see https://flutter.io/docs/development/data-and-backend/json#manual-encoding

@JsonSerializable()
class Pokemon {
  final int id;
  final String number;
  final String name;
  final String img;
  final List<String> type;

  Pokemon(
      {@required this.id,
      @required this.number,
      @required this.name,
      @required this.img,
      @required this.type});

  factory Pokemon.fromJson(Map<String, dynamic> json) =>
      _$PokemonFromJson(json);

  Map<String, dynamic> toJson() => _$PokemonToJson(this);
}
