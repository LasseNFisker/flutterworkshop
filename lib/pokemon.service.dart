import 'dart:convert';
import 'package:rxdart/rxdart.dart';
import 'package:workshop/pokemon.model.dart';
import 'package:http/http.dart' as http;

class PokemonService {
  BehaviorSubject<String> _searchTerm$ = new BehaviorSubject(seedValue: "");

  BehaviorSubject<List<Pokemon>> _pokemons$ =
      new BehaviorSubject(seedValue: null);

  Observable<List<Pokemon>> get pokemons$ {
    if (_pokemons$.value == null) {
      _fetchPokemon();
    }

    // return _pokemons$.asBroadcastStream();
    return Observable.combineLatest2<List<Pokemon>, String, List<Pokemon>>(
        _pokemons$, _searchTerm$, (pokemons, term) {
      var filtered = pokemons.where((pokemon) {
        return pokemon.name.toLowerCase().contains(term.toLowerCase());
      }).toList();
      return filtered;
    }).asBroadcastStream();
  }

  filterPokemons(String term) => this._searchTerm$.add(term);

  _fetchPokemon() => http
          .get(
              "https://raw.githubusercontent.com/Biuni/PokemonGO-Pokedex/master/pokedex.json")
          .then((res) {
        if (res == null || res.body == null) {
          return List<Pokemon>();
        }

        var responseJson = json.decode(res.body);

        List<Pokemon> pokemons = (responseJson['pokemon'] as List)
            .map((pokeJson) => Pokemon.fromJson(pokeJson))
            .toList();

        this._pokemons$.add(pokemons);
      });
}

final PokemonService pokemonService = PokemonService();
