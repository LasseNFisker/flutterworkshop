import 'package:flutter/material.dart';
import 'package:workshop/details/details.page.dart';
import 'package:workshop/pokemon.model.dart';

class AppPokemon extends StatelessWidget {
  static final double height = 90;
  final Pokemon pokemon;

  const AppPokemon({Key key, @required this.pokemon}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      margin: EdgeInsets.symmetric(vertical: 10),
      height: height,
      child: GestureDetector(
          onTap: () => Navigator.of(context).push(MaterialPageRoute(
              builder: (context) => PokemonDetailsPage(pokemon: pokemon))),
          child: GridTile(
              child: Column(
            children: <Widget>[
              Hero(
                tag: pokemon.id,
                child: Container(
                    height: height,
                    width: height,
                    decoration: BoxDecoration(
                      color: Colors.blueGrey.shade600,
                      shape: BoxShape.circle,
                      image: DecorationImage(
                          fit: BoxFit.fill, image: NetworkImage(pokemon.img)),
                    )),
              ),
              Text(
                pokemon.name,
                style: TextStyle(color: Colors.white, fontSize: 20),
              ),
              Text(
                pokemon.number.toString(),
                style: TextStyle(color: Colors.grey.shade400, fontSize: 19),
              ),
            ],
          ))),
    );
  }
}
